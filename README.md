-> Declaração das variaveis 
-> Criando constantes dos pinos da esp para configuração de envio e recebimento de dados
-> Criando constantes para as keys de valores de dados recebidos pelo bluetoth


```c{.line-numbers} 
#include <ArduinoOTA.h>
#include <Wire.h>
#include <BluetoothSerial.h>

/////////////////BLUETOOTH//////////////////////

BluetoothSerial ESP_BT;

String btCommand;

const char BLUETOOTH_END_SIGNAL = '*';

const String TOP_LEFT_PROPELLER_SPEED = "sp_tl=";
const String TOP_RIGHT_PROPELLER_SPEED = "sp_tr=";
const String BOTTOM_LEFT_PROPELLER_SPEED = "sp_bl=";
const String BOTTOM_RIGHT_PROPELLER_SPEED = "sp_br=";

const String KP = "kp=";
const String KI = "ki=";
const String KD = "kd=";

const String TURN_ON = "on";
const String TURN_OFF = "off";

const String ENABLE_PID = "e_pid";
const String DISABLE_PID = "d_pid";

const String SAMPLE_TIME = "s_time="; 

//////////////////////////////////////////////////////////

////////////////////PIN CONSTANTS/////////////////////////

const int TOP_LEFT_PROPELLER = 15;
const int TOP_RIGHT_PROPELLER = 13;
const int BOTTOM_LEFT_PROPELLER = 23;
const int BOTTOM_RIGHT_PROPELLER = 32;

const int BLUE_LED = 2;

//////////////////////////////////////////////////////////

////////////////////////PID///////////////////////////////

double sampleTime, lastTimePID, elapsedTimePID;
double lastInput, input;
float PID, pwmLeft, pwmRight, error, errSum, previousError;

float pid_p=0;
float pid_i=0;
float pid_d=0;

double kp=0;//3.55
double ki=0;//0.003
double kd=0;//2.05

///////////////////////////////////////////////////////////

////////////////////////SPEED//////////////////////////////

double topRightSpeed = 0;
double topLeftSpeed = 0;
double bottomRightSpeed = 0;
double bottomLeftSpeed = 0;

double pwmTR = 0;
double pwmTL = 0;
double pwmBR = 0;
double pwmBL = 0;

///////////////////////////////////////////////////////////

///////////////////////ACCELEROMETER///////////////////////

float elapsedTime, currentTime, timePrev;

int16_t accRawX, accRawY, accRawZ, gyrRawX, gyrRawY, gyrRawZ;

float accelerationAngle[2], gyroAngle[2], totalAngle[2];

float angleFromAccX, angleFromAccY;
float angleFromGyrX, angleFromGyrY;
float angleTotalX, angleTotalY;
float desiredAngle = 0;

const float radToDeg  = 180/3.141592654;

////////////////////////////////////////////////////////////

///////////////////////CONTROL VARS/////////////////////////

bool isDroneOn = false;
bool isPIDOn = false;

////////////////////////////////////////////////////////////
```

-> Configuração dos pinos da esp e canais de comunicação da pwm.
```c
void setup() {

  Serial.begin(115200);
  
  Wire.begin();
  Wire.beginTransmission(0x68);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

  currentTime = millis();

  ESP_BT.begin("Sadness and Drone");

  pinMode(2, OUTPUT);//Specify that LED pin is output
  pinMode(13, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(23, OUTPUT);
  pinMode(32, OUTPUT);

  // configuracao pwm
  ledcAttachPin(TOP_RIGHT_PROPELLER, 0);
  ledcAttachPin(TOP_LEFT_PROPELLER, 1);
  ledcAttachPin(BOTTOM_RIGHT_PROPELLER, 2);
  ledcAttachPin(BOTTOM_LEFT_PROPELLER, 3);
  
  ledcSetup(0, 5000, 10);
  ledcSetup(1, 5000, 10);
  ledcSetup(2, 5000, 10);
  ledcSetup(3, 5000, 10);
}
```


-> loop de verificação dos dados do acelerometro, calculo dos PID's e dados recebidos pelo bluetooth.

```c
void loop() {
  
  handleAccelerometer();
  
  handlePIDX();
  handlePIDY();
  
  checkBluetooth();
}
```

-> Função para verificar se foi recebido algo do bluetooth e redirecionar os dados recebidos para a função responsavel pelo tratamento dos dados recebidos pelo bluetooth
```c 
void checkBluetooth() {
  if (ESP_BT.available()) {
    receiveBluetoothCommand(ESP_BT.read());
  }
}
```

-> função de tratamento de dados recebidos do bluetoth,
-> controle de velocidade de cada helice
-> liga/desliga drone
-> liga/desliga pid
-> seta valor de ki, kp, kd (com melhoria)
-> seta o valor do sample time (com melhoria)
```c
void receiveBluetoothCommand(char incoming) {
  
  if (incoming != BLUETOOTH_END_SIGNAL) {
    btCommand += incoming;
  } else {
     if (incoming != BLUETOOTH_END_SIGNAL) {
    btCommand += incoming;
  } else {
    if(btCommand.startsWith(TOP_LEFT_PROPELLER_SPEED)) {
      if (isDroneOn) {
        topLeftSpeed = btCommand.substring(6).toDouble();
        ledcWrite(1, topLeftSpeed);
      }
    } else if (btCommand.startsWith(TOP_RIGHT_PROPELLER_SPEED)) {
      if (isDroneOn) {
        topRightSpeed = btCommand.substring(6).toDouble();
        ledcWrite(0, topRightSpeed);
      }
    } else if (btCommand.startsWith(BOTTOM_LEFT_PROPELLER_SPEED)) {
      if (isDroneOn) {
        bottomLeftSpeed = btCommand.substring(6).toDouble();
        ledcWrite(3, bottomLeftSpeed);
      }
    } else if (btCommand.startsWith(BOTTOM_RIGHT_PROPELLER_SPEED)) {
      if (isDroneOn) {
        bottomRightSpeed = btCommand.substring(6).toDouble();
        ledcWrite(2, bottomRightSpeed);
      }
    } else if (btCommand.startsWith(KP)) {
      kp = btCommand.substring(3).toDouble();
    } else if (btCommand.startsWith(KI)) {
      ki = btCommand.substring(3).toDouble() * (sampleTime / 1000) ;
    } else if (btCommand.startsWith(KD)) {
      kd = btCommand.substring(3).toDouble() / (sampleTime / 1000);
    } else if (btCommand.startsWith(TURN_ON)) {
      digitalWrite(BLUE_LED, HIGH);
      isDroneOn = true;
    } else if (btCommand.startsWith(TURN_OFF)) {
      digitalWrite(BLUE_LED, LOW);
      isDroneOn = false;
      ledcWrite(0, 0);
      ledcWrite(1, 0);
      ledcWrite(2, 0);
      ledcWrite(3, 0);
    } else if (btCommand.startsWith(ENABLE_PID)) {
      isPIDOn = true;
    } else if (btCommand.startsWith(DISABLE_PID)) {
      isPIDOn = false;
    } else if (btCommand.startsWith(SAMPLE_TIME)) {
      double newSampleTime = btCommand.substring(7).toDouble();
      if (newSampleTime > 0){
        double ratio  = newSampleTime / sampleTime;
        ki *= ratio;
        kd /= ratio;
        sampleTime = newSampleTime;
      }
    }
    btCommand = "";
  }
}
```

-> Calculo do pid do eixo x já com as melhorias
```c
void handlePIDX() {

  elapsedTimePID = millis() - lastTimePID;

  if (isPIDOn && elapsedTimePID >= sampleTime) {

    input = totalAngle[0];
    error = desiredAngle - input;
    errSum += error;

    double dInput = (input - lastInput);
    lastInput = input;
    
    pid_p = kp * error;
    pid_i = ki * errSum;  
    pid_d = kd * dInput;
    
    PID = pid_p + pid_i + pid_d;
    
    pwmTR = topRightSpeed - PID;
    pwmTL = topLeftSpeed - PID;
    pwmBL = bottomLeftSpeed + PID;
    pwmBR = bottomRightSpeed + PID;
  
    if (isDroneOn) {
      ledcWrite(3, pwmTR);
      ledcWrite(0, pwmBL);
    }
  
    previousError = error;
    lastTimePID = millis();  
  }

}
```

-> Calculo do pid do eixo y já com as melhorias
```c
void handlePIDY() {

  elapsedTimePID = millis() - lastTimePID;

  if (isPIDOn && elapsedTimePID >= sampleTime) {

    input = totalAngle[1];
    error = desiredAngle - input;
    errSum += error;

    double dInput = (input - lastInput);
    lastInput = input;
    
    pid_p = kp * error;
    pid_i = ki * errSum;  
    pid_d = kd * dInput;
    
    PID = pid_p + pid_i + pid_d;
    
    pwmTR = topRightSpeed - PID;
    pwmTL = topLeftSpeed + PID;
    pwmBL = bottomLeftSpeed + PID;
    pwmBR = bottomRightSpeed - PID;
  
    if (isDroneOn) {
      ledcWrite(0, pwmTR);
      ledcWrite(1, pwmTL);
      ledcWrite(2, pwmBR);
      ledcWrite(3, pwmBL);
    }
  
    previousError = error;
    lastTimePID = millis();  
  }
```

-> Função responsavel por tratar os dados recebidos do acelerometro.
-> A função também recebe os valores de x e y do acelerometro, aplica o filtro complementar e retorna o novo valor de x e y tratado.
-> Os dados são recebidos a partir de uma comunicação i2c

```c
void handleAccelerometer() {

  timePrev = currentTime;
  currentTime = millis();
  elapsedTime = (currentTime - timePrev) / 1000;

  Wire.beginTransmission(0x68);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(0x68,6,true); 
  
  accRawX = Wire.read() << 8|Wire.read();
  accRawY = Wire.read() << 8|Wire.read();
  accRawZ = Wire.read() << 8|Wire.read();

  // x
  accelerationAngle[0] = atan((accRawY/16384.0)/sqrt(pow((accRawX/16384.0),2) + pow((accRawZ/16384.0),2)))*radToDeg;
  // y
  accelerationAngle[1] = atan(-1*(accRawX/16384.0)/sqrt(pow((accRawY/16384.0),2) + pow((accRawZ/16384.0),2)))*radToDeg;

  Wire.beginTransmission(0x68);
  Wire.write(0x43); 
  Wire.endTransmission(false);
  Wire.requestFrom(0x68,4,true); 
  
  gyrRawX=Wire.read()<<8|Wire.read(); 
  gyrRawY=Wire.read()<<8|Wire.read();
  
  // x
  gyroAngle[0] = gyrRawX/131.0; 
  // y
  gyroAngle[1] = gyrRawY/131.0;
  
  // x
  totalAngle[0] = 0.98 *(totalAngle[0] + gyroAngle[0]*elapsedTime) + 0.02*accelerationAngle[0];
  // y
  totalAngle[1] = 0.98 *(totalAngle[1] + gyroAngle[1]*elapsedTime) + 0.02*accelerationAngle[1];
}

```
